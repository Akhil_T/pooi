use clap::{Arg, ArgAction, Command, ValueHint};

pub fn build(_selectors: &[&str]) -> Command {
    Command::new("pooi!")
        .author("Vysakh Premkumar vysakhpr218@gmail.com>")
        .about("please use --help for more detailed information")
        .long_about("trivia on the command line.")
        .version("0.2.1")
        .help_template("{bin}\n{about}\n\n{usage-heading}\n    {usage}\n\n{all-args}\n\nversion {version} by {author}\nplease report any bugs to https://github.com/tellmeY18/pooi/issues")
        .arg(Arg::new("all")
            .short('a')
            .long("all")
            .display_order(1)
            .action(ArgAction::SetTrue)
            .help("Prints all of the answers found")
        )
        .arg(Arg::new("urls")
            .short('u')
            .long("urls")
            .display_order(2)
            .help("Also print a list of the top urls associated with your query")
            .action(ArgAction::SetTrue)
            .long_help(
                "Also print a list of the top urls associated with your query\n\
                these are typically only shown when an answer can't be found"
            )
        )
        .arg(Arg::new("quiet")
            .short('q')
            .long("quiet")
            .display_order(3)
            .action(ArgAction::SetTrue)
            .help("Only print the answer (if applicable) and error messages")
            .long_help(
                "Only print the answer (if applicable) and error messages\n\
                silences corrections, unrequested urls and selector information"
            )
        )
        .arg(Arg::new("raw")
            .short('r')
            .long("raw")
            .display_order(4)
            .action(ArgAction::SetTrue)
            .help("Raw output (use --help for details)")
            .long_help(
                "Raw output - no colours, terminal attributes and messages\n\
                this is only required if you don't want to use colours etc in your terminal\n\
                if you are piping the output somewhere else this flag is passed automatically"
            )
        )
        .arg(Arg::new("save")
            .short('s')
            .long("save")
            .display_order(5)
            .conflicts_with("cache")
            .action(ArgAction::SetTrue)
            .help("Saves the raw HTML for this query")
            .long_help(
                "Saves the raw HTML for this query to the following path:\n\
                (BSD/Linux) $HOME/.cache/pooi/[date]-[query].html\n\
                (MacOS)     $HOME/Library/Application Support/pooi/[date]-[query].html\n\
                (Windows)   %LOCALAPPDATA%\\pooi\\[date]-[query].html"
            )
        )
        .arg(Arg::new("cache")
            .short('c')
            .long("cache")
            .display_order(6)
            .action(ArgAction::SetTrue)
            .conflicts_with("language")
            .help("Use the most recent cached HTML")
        )
        .arg(Arg::new("clean")
            .long("clean")
            .exclusive(true)
            .display_order(7)
            .action(ArgAction::SetTrue)
            .help("Remove all previously saved results")
        )
        .arg(Arg::new("list")
            .short('L')
            .long("list")
            .exclusive(true)
            .display_order(8)
            .action(ArgAction::SetTrue)
            .help("Prints a table of all the valid answer selectors")
            .long_help(
                "Prints a table of all the valid answer selectors\n\
                includes descriptions and examples (for use with the -p --pick option)"
            )
        )
        .arg(Arg::new("language")
            .short('l')
            .long("lang")
            .value_name("LANGUAGE")
            .display_order(9)
            .conflicts_with("cache")
            .action(ArgAction::Set)
            .help("Specify the language to use (eg: en-GB)")
            .long_help(
                "Specify the language to use (eg: en-GB)\n\
                pooi uses your system language by default\n\
                if that can't be resolved then it will default to en-US"
            )
            .value_hint(ValueHint::Other)
        )
        .arg(Arg::new("selectors")
            .short('p')
            .long("pick")
            .value_name("SELECTOR")
            .num_args(1..)
            .value_delimiter(',')
            .hide_possible_values(true)
            .display_order(10)
            .action(ArgAction::Set)
            .help("Target specific answers, use -- to stop parsing arguments")
            .long_help(
                "Target specific answers, use -- to stop parsing arguments\n\
                eg: pooi -p basic1 basic2 summary -- my search query"
            )
        )
        .arg(Arg::new("query")
            .conflicts_with_all(&["cache", "clean", "list"])
            .help("Whaddya wanna know?")
            .required_unless_present_any(&["cache", "clean", "list"])
            .num_args(1..)
            .value_hint(ValueHint::Other)
        )
}
