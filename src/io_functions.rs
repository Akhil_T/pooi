use std::{env, fs, io::Write, path::Path};

use anyhow::{bail, Result};
use chrono::prelude::Local;
use glob::glob;
use whoami::{platform, Platform};

pub fn fetch(query: String, lang: String) -> Result<String> {
    let response = ureq::get("https://google.com/search")
        .set(
            "User-Agent",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) QtWebEngine/5.15.2 Chrome/87.0.4280.144 Safari/537.36",
        )
        .query("q", &query)
        .query("hl", &lang)
        .call()?
        .into_string()?;
    Ok(response)
}

pub fn cached_html() -> Result<String> {
    let os_type = platform();
    let files = get_file_list(&os_type)?;
    let html = fs::read_to_string(&files[files.len() - 1])?;
    Ok(html)
}

pub fn save_html(query: &[&str], html: &str) -> Result<String> {
    let os_type = platform();
    let cache_path = get_cache_path(&os_type)?;
    let file_date = Local::now().format("%Y%m%d%H%M%S").to_string();
    let file_query = query.join("_");
    let sep = sep_type(&os_type);

    let cache_dir = format!("{}{}pooi", cache_path, sep);
    if !Path::new(&cache_dir).exists() {
        fs::create_dir_all(&cache_dir)?;
    }

    let full_path = format!("{}{}{}-{}.html", cache_dir, sep, file_date, file_query);
    let mut file = fs::File::create(&full_path)?;
    file.write_all(html.as_bytes())?;
    Ok(full_path)
}

pub fn clean_cache() -> Result<String> {
    let os_type = platform();
    let sep = sep_type(&os_type);
    let target = format!("{}{}pooi", get_cache_path(&os_type)?, sep);
    fs::remove_dir_all(&target)?;
    Ok(target)
}

fn sep_type(os_type: &Platform) -> &str {
    match os_type {
        Platform::Windows => "\\",
        _ => "/",
    }
}

fn get_cache_path(os_type: &Platform) -> Result<String> {
    let cache_path = match os_type {
        Platform::Bsd | Platform::Linux => env::var("XDG_CACHE_HOME").unwrap_or_else(|_| {
            format!(
                "{}/.cache",
                env::var("HOME").expect("HOME environment variable not set")
            )
        }),
        Platform::MacOS => {
            format!(
                "{}/Library/Application Support",
                env::var("HOME").expect("HOME environment variable not set")
            )
        }
        Platform::Windows => env::var("LOCALAPPDATA")?,
        _ => bail!("This feature is not supported on your platform, sorry!"),
    };
    Ok(cache_path)
}

fn get_file_list(os_type: &Platform) -> Result<Vec<String>> {
    let sep = sep_type(os_type);
    let cache_path = format!("{}{}pooi{}*.html", get_cache_path(os_type)?, sep, sep);

    let files: Vec<String> = glob(&cache_path)?
        .filter_map(Result::ok)
        .map(|path| path.display().to_string())
        .collect();

    if files.is_empty() {
        bail!("Can't find any cached results, sorry!");
    }
    Ok(files)
}
