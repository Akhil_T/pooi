use crate::selectors::{default_output, get_vec};
use scraper::Html;

pub fn main(data: &Html, tty: &bool, w: usize) {
    // Extract the content from the specified selector
    let extracted_data = get_vec(data, "span.hgKElc");

    // Join the extracted content into a single string
    let joined_data = extracted_data.join("");

    // Output the result based on the TTY flag
    match tty {
        true => default_output(&joined_data, w),
        false => println!("{}", joined_data),
    }
}
